import numpy as np
from sklearn.svm import SVC
from sklearn import metrics
from sklearn.model_selection import KFold
import matplotlib.pyplot as plt
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import cross_val_predict
from sklearn.multiclass import OneVsOneClassifier;
from sklearn.multiclass import OneVsRestClassifier;
from scipy.stats import itemfreq;

# Read Data from file
train_file_path = "data/features.train";
test_file_path = "data/features.test";
train = np.loadtxt(train_file_path);
test = np.loadtxt(test_file_path);
kf = KFold(shuffle=True, n_splits=10);


def printMetrics(y_true, y_pred):
    accuracy = metrics.accuracy_score(y_true, y_pred);
    print('Total Accuracy: ', accuracy * 100);
    print("Error: ", 1 - accuracy);
    # print confusion matrix
    CM = metrics.confusion_matrix(y_true, y_pred);
    print("Confusion Matrix:", end="\n");
    print(CM, end=' \n\n');
    # print classification report
    # target_names = ''.join(str(x) for x in range(10));
    # print(metrics.classification_report(y_true, y_pred, target_names=target_names));
    # Class Accuracy
    print("Printing Class Accuracy", end="\n\n");
    for i in range(2):
        print("Class %d accuracy is %.2f%%" % (i, CM[i, i] / sum(CM[i, :]) * 100), end='\n\n');


def trainAndPredict(model, classification_type, test_type='test', ovr=None, ovo=None, cross_fold_enabled= False):
    #print(model)
    #Preprocessing
    train_X = train[:, 1:3]
    train_Y = train[:, 0].astype(int)
    test_X = test[:, 1:3]
    test_Y = test[:, 0].astype(int)

    if (classification_type == 'ovr'):
        train_Y[train_Y == ovr] = -1
        train_Y[train_Y != -1] = 1
        test_Y[test_Y == ovr] = -1
        test_Y[test_Y != -1] = 1
    elif (classification_type == 'ovo'):
        train_ids = [i for i, v in enumerate(train[:,0]) if (v == ovo[0]) | (v == ovo[1])]
        test_ids = [i for i, v in enumerate(test[:,0]) if (v == ovo[0]) | (v == ovo[1])]
        train_X = train_X[train_ids]
        train_Y = train_Y[train_ids]
        test_X = test_X[test_ids]
        test_Y = test_Y[test_ids]
        train_Y[train_Y == ovo[0]] = -1
        train_Y[train_Y == ovo[1]] = 1
        test_Y[test_Y == ovo[0]] = -1
        test_Y[test_Y == ovo[1]] = 1
    y_true = test_Y;
    x_test = test_X;
    if (test_type == 'train'):
        y_true = train_Y;
        x_test = train_X;


    if cross_fold_enabled:
        ecv = np.array([])
        for i in range(100):
            ter = np.array([])
            for train_index, test_index in kf.split(train_X):
                acc = model.fit(train_X[train_index], train_Y[train_index]).score(train_X[test_index],
                                                                                  train_Y[test_index])
                ter = np.append(ter, 1-acc)
            ecv = np.append(ecv, ter.mean())
        print("ecv mean :", ecv.mean())
    else:
        model.fit(train_X,train_Y)
    y_pred = model.predict(x_test)
    print("{} Error for {} classificaiton is {}".format(test_type, "{} vs all".format(
           ovr) if classification_type == "ovr" else "{} vs {}".format(ovo[0], ovo[1]),
                                                          (1 - metrics.accuracy_score(y_true, y_pred))))


   # plt.contourf()



    #Train Data
    #cvs = cross_val_score(model,train_X,train_Y,cv=10)
    #print(cvs)
    #model.fit(train_X, train_Y);

    #Predict data
    #y_pred = model.predict(x_test);
    #print("Score: ",model.score(x_test,y_true));

    #Metrics
    #print(y_pred);
    #print(itemfreq(y_pred))
    #printMetrics(y_true,y_pred)
    #print("{} Error for {} classificaiton is {}".format(test_type,"{} vs all".format(ovr) if classification_type == "ovr" else "{} vs {}".format(ovo[0],ovo[1]),(1-metrics.accuracy_score(y_true,y_pred))));

def do_test():
    # problem 2.2 - Polynomial Kernels
    # Problem 2.2.1
    ovr_set1 = np.array([0, 2, 4, 6, 8]);
    for ovr in ovr_set1:
        pclf1 = SVC(kernel='poly', degree=2, coef0=1, gamma=1, C=0.01)
        trainAndPredict(model=pclf1, classification_type='ovr', ovr=ovr, test_type='train');
        print(np.size(pclf1.support_vectors_))

    # problem 2.2.2
    print("problem 2.2.2")
    ovr_set2 = np.array([1, 3, 5, 7, 9]);
    for ovr in ovr_set2:
        pclf2 = SVC(kernel='poly', degree=2, coef0=1, gamma=1, C=0.01)
        trainAndPredict(model=pclf2, classification_type='ovr', ovr=ovr, test_type='train');
        print(np.size(pclf2.support_vectors_))

    # problem 2.2.4 & 2.2.5
    print("Problem 2.2.4")
    ovo_set1 = [1, 5]
    q_vals = [2, 5]
    c_vals = [0.001, 0.01, 0.1, 1]
    for degree in q_vals:
        for c in c_vals:
            pclf = SVC(kernel='poly', degree=degree, coef0=1, gamma=1, C=c);
            trainAndPredict(model=pclf, classification_type='ovo', ovo=ovo_set1, test_type='train');
            trainAndPredict(model=pclf, classification_type='ovo', ovo=ovo_set1, test_type='test');
            print(np.size(pclf.support_vectors_))

    # problem 2.3.1
    ovo_set1 = [1, 5]
    print("#### Problem 2.3.1####")
    c_vals = [0.0001, 0.001, 0.01, 0.1, 1]
    q_vals = [2]
    for degree in q_vals:
        for c in c_vals:
            pclf = SVC(kernel='poly', degree=degree, coef0=1, gamma=1, C=c);
            trainAndPredict(model=pclf, classification_type='ovo', ovo=ovo_set1, test_type='test',
                            cross_fold_enabled=True)
    # problem 2.3.2
    pclf = SVC(kernel='poly', degree=2, coef0=1, gamma=1, C=0.01);
    trainAndPredict(model=pclf, classification_type='ovo', ovo=ovo_set1, test_type='test', cross_fold_enabled=True)

    #problem 2.4
    print("Problem 2.3.4")
    c_vals = [0.01,1,100,10e4,10e6]
    #2.4.1
    for c in c_vals:
        gclf = SVC(kernel='rbf', gamma=1, C=c)
        trainAndPredict(model=gclf, classification_type='ovo', ovo=ovo_set1, test_type='train')
    #2.4.2
    for c in c_vals:
        gclf = SVC(kernel='rbf', gamma=1, C=c)
        trainAndPredict(model=pclf, classification_type='ovo', ovo=ovo_set1, test_type='test')

if __name__ == '__main__':
    print("Problem 2.3.4")
    ovo_set1=[1,5]
    c_vals = [0.01, 1, 100, 10e4, 10e6]
    # 2.4.1
    for c in c_vals:
        gclf = SVC(kernel='rbf', gamma=1, C=c)
        trainAndPredict(model=gclf, classification_type='ovo', ovo=ovo_set1, test_type='test')